from django.urls import path
from .views import story8,data

app_name = 'story8'

urlpatterns = [
    path('story8', story8),
    path('story8/data', data),
]