from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import story8
import unittest
import time

# Create your tests here.
class Lab9_Test(TestCase):
    def test_url_exists(self):
        response = Client().get('/story8')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/story8/data')
        self.assertEqual(response.status_code, 200)

    def test_using_booklist_template(self):
        response = Client().get('/story8')
        self.assertTemplateUsed(response, 'story8.html')

    def test_using_homepage_func(self):
        found = resolve('/story8')
        self.assertEqual(found.func, story8)
    


class TestWithSelenium(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',options=chrome_options)

    def test_konten(self):
        self.driver.get("http://localhost:8000/story8")
        self.assertIn("Book List", self.driver.page_source)

    def tearDown(self):
        self.driver.quit
