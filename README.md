# Cerita Abis UTS
This Gitlab repository is the result of the work from Mohammed Al Kwarizmi

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/alkwrzm/cerita-setelah-UTS/badges/master/pipeline.svg)](https://gitlab.com/alkwrzm/cerita-setelah-UTS/commits/master)
[![coverage report](https://gitlab.com/alkwrzm/cerita-setelah-UTS/badges/master/coverage.svg)](https://gitlab.com/alkwrzm/cerita-setelah-UTS/commits/master)

## Heroku
Click [here](http://cerita-al-abis-uts.herokuapp.com) to see my website.

## Note (For story 9)

- Username: admin
- Password: admin
