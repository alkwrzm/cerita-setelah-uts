from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
import unittest
from StatusKu.views import homepage
from .models import Activity
from .forms import ActivityForm
import time
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class TestingUrls(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get('/status')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/status')
        self.assertEqual(found.func, homepage)

class TestingModels(TestCase):

    def test_apakah_dapat_membuat_aktifitas(self):
        new_activity = Activity.objects.create(text = "Test Message Status")
        counting_all_available_activity = Activity.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_aktivity_post_berhasil(self):
        message = 'Hai, ini test'
        response_post = Client().post('/status', {'text': message})
        self.assertEqual(response_post.status_code, 302)

class TestingViews(TestCase):
    def test_does_views_render_the_correct_templates(self):
        response = self.client.get('/status')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_can_we_create_object_without_fill_anything(self):
        form = ActivityForm(data={'message' : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['message'], ['This field is required.'])
    
    def test_does_objects_that_we_create_shows_up(self):
        response = self.client.post('/status', data={'message' : 'sedang belajar'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'sedang belajar')

class TestWithSelenium(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',options=chrome_options)

    def test_masukkan(self):
        self.driver.get("http://localhost:8000/status")
        self.assertIn("Perasaanmu...", self.driver.page_source)
        self.assertIn("Bagaimana Kabarmu", self.driver.page_source)
        self.driver.find_element_by_id('id_message').send_keys("coba-coba")
        self.driver.find_element_by_id('submit').click()
        self.assertIn("http://localhost:8000/status", self.driver.current_url)

    def tearDown(self):
        self.driver.quit

        