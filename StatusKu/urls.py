from django.urls import path
from .views import homepage

app_name = 'StatusKu'

urlpatterns = [
    path('status', homepage)
]