from django import forms
from . import models

class ActivityForm(forms.Form):
	message = forms.CharField(label = "Status", widget=forms.TextInput(attrs = {
	'placeholder' : 'Kabarmu?',
	'type' : 'text',
    'required' : True
}))
