from django.db import models

# Create your models here.

class Activity(models.Model) :
    text = models.TextField(max_length=100, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)