from django.shortcuts import render, redirect
from .models import Activity
from .forms import ActivityForm

# Create your views here.

def homepage(request):
    if request.method == 'POST' :
        form = ActivityForm(request.POST)
        if (form.is_valid()) :
            db = Activity()
            db.text = form.cleaned_data['message']
            db.save()
        return redirect('/status')
    else :
        form = ActivityForm()
        db = Activity.objects.all().order_by('-id')
        response = {
            'form' : form,
            'status' : db
        }
    return render(request, 'homepage.html', response)
