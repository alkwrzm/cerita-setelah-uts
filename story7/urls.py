from django.urls import path
from .views import story7

app_name = 'story7'

urlpatterns = [
    path('story7', story7)
]