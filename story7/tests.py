from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
import unittest
from .views import story7
import time
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

# Create your tests here.
class TestingUrls(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get('/story7')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/story7')
        self.assertEqual(found.func, story7)

class TestingViews(TestCase):
    def test_does_views_render_the_correct_templates(self):
        response = self.client.get('/story7')
        self.assertTemplateUsed(response, 'story7.html')


class TestWithSelenium(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',options=chrome_options)

    def test_js_accordian_one(self):
        self.driver.get("http://localhost:8000/story7")
        self.driver.find_element_by_id('accordion-1').click()
        self.assertIn("Seperti mahasiswa pada umumnya", self.driver.page_source)

    def test_js_accordian_two(self):
        self.driver.get("http://localhost:8000/story7")
        self.driver.find_element_by_id('accordion-2').click()
        self.assertIn("Compfest 11 - VPic Equipment and Security", self.driver.page_source)

    def test_js_accordian_three(self):
        self.driver.get("http://localhost:8000/story7")
        self.driver.find_element_by_id('accordion-3').click()
        self.assertIn("Juara 1 lomba masukin kerupuk ke botol dalam botol paku", self.driver.page_source)

    def test_change(self):
        self.driver.get("http://localhost:8000/story7")
        body = self.driver.find_element_by_css_selector('body')
        color = body.value_of_css_property('background')
        text = self.driver.find_element_by_id('nama').value_of_css_property('color')
        text1 = self.driver.find_element_by_css_selector('p').value_of_css_property('color')
        text2 = self.driver.find_element_by_css_selector('li').value_of_css_property('color')
        text3 = self.driver.find_element_by_id('nav').value_of_css_property('color')
        text4 = self.driver.find_element_by_id('center').value_of_css_property('border-color')
        self.assertEqual("rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box", color)
        self.assertEqual("rgba(0, 0, 0, 1)" , text)
        self.assertEqual("rgba(0, 0, 0, 1)", text1)
        self.assertEqual("rgba(0, 0, 0, 1)", text2)
        self.assertEqual("rgba(0, 0, 0, 1)", text3)
        self.assertEqual("rgb(0, 0, 0)", text4)
        self.driver.find_element_by_id('button').click()
        color2 = body.value_of_css_property('background')
        text = self.driver.find_element_by_id('nama').value_of_css_property('color')
        text1 = self.driver.find_element_by_css_selector('p').value_of_css_property('color')
        text2 = self.driver.find_element_by_css_selector('li').value_of_css_property('color')
        text3 = self.driver.find_element_by_id('nav').value_of_css_property('color')
        text4 = self.driver.find_element_by_id('center').value_of_css_property('border-color')
        self.assertEqual("rgb(0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box", color2)
        self.assertEqual("rgba(255, 255, 255, 1)", text)
        self.assertEqual("rgba(255, 255, 255, 1)", text1)
        self.assertEqual("rgba(255, 255, 255, 1)", text2)
        self.assertEqual("rgba(255, 255, 255, 1)", text3)
        self.assertEqual("rgb(255, 255, 255)", text4)

    def tearDown(self):
        self.driver.quit
