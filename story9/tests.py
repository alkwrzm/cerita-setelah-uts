from django.test import TestCase, Client
import unittest
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from .views import story9
# Create your tests here.

class Story9(TestCase):

    def test_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'story9.html')
    
    def test_header(self):
        request = HttpRequest()
        response = story9(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello!",html_response)


class TestingViews(TestCase):
    
    def test_does_views_render_the_correct_templates(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_function(self):
        response = self.client.post('/',data={'username':'username','password':'password'})
        self.assertEqual(response.status_code,302)

    def test_benar(self):
        self.client.post('/',data={'username':'admin','password':'admin'})
        response = Client().get('/status')
        self.assertTemplateUsed(response,'homepage.html')

    def test_salah(self):
        self.client.post('/',data={'username':'username','password':'password'})
        response = Client().get('/')
        self.assertTemplateUsed(response,'story9.html')



class FuncTest9(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',options=chrome_options)
        self.driver.get('http://localhost:8000/')

    def tearDown(self):
        self.driver.quit
    
    def test_header_page_before_login(self):
        self.assertIn("Hello!",self.driver.page_source)
    
    def test_header_page_after_login(self):
        browser = self.driver
        browser.find_element_by_xpath('//*[@id="id_username"]').send_keys("admin")
        browser.find_element_by_xpath('//*[@id="id_password"]').send_keys("admin")
        browser.find_element_by_xpath('//*[@id="submit"]').click()
        self.assertIn("Bagaimana Kabarmu admin?",browser.page_source)
        browser.find_element_by_id('nav').click()
        browser.find_element_by_id('logot').click()
        self.assertIn("Hello!",browser.page_source)
        response = Client().get('/')
        self.assertTemplateUsed(response,'story9.html')
        self.assertEqual(response.status_code,200)
