from django.urls import path
from .views import story9, logout_views


appname = 'story9'

urlpatterns = [
   path('', story9, name = 'startPage'),
   path('story9/logout',logout_views, name='logout')
]