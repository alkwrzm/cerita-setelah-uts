$(document).ready(function() {

    cariBuku('1');

    $(function() {
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }


        var changed = true;
        $("#button").click(function() {
            if (changed) {
                console.log("theme changed");
                $("body").css('background', 'black');
                $("h1").css('color', 'white');
                $("#center").css('border-color', 'white');
                $("p").css('color', 'white');
                $("li").css('color', 'white');
                $("#nav").css('color', 'white');
                changed = false;
            } else {
                console.log("back to original");
                $("body").css('background', 'white');
                $("h1").css('color', 'black');
                $("#center").css('border-color', 'black');
                $("p").css('color', 'black');
                $("li").css('color', 'black');
                $("#nav").css('color', 'black');
                changed = true;
            }
        });
        $("#button-buku").click(function() {
            cariBuku(document.getElementById('input').value);
        });

    });

});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

function cariBuku(param) {
    console.log(param)
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + param,
        success: function(data) {
            $('#konten').html('')
            var result = '<tr>';
            for (var i = 0; i < data.items.length; i++) {
                try {
                    if (data.items[i].volumeInfo.imageLinks == undefined) {
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td style='text-align = center'><strong>no image" +
                            "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>";
                    } else {
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                            "<td><img class='img-fluid' style='width:22vh' src='" +
                            data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>";
                    }

                } catch (error) {

                }
            }
            $('#konten').append(result);
        },
        error: function(error) {
            alert("Books not found");
        }
    })
}